package calculator.string.helper;

import calculator.string.parser.StringCalculatorParser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StringCalculatorHelperTest {

    static final String OK_2 = "1,1";
    static final String OK_3 = "//;\n1;2";
    static final String OK_4 = "1,1,2";
    static final String OK_6 = "1\n2,3";
    static final String OK_15 = "1,2,3,4,5";
    static final String NOT_OK = "1,\n";

    @Test
    void getDelimiter_given_one_one_should_be_absent() {
        assertFalse(StringCalculatorParser.getDelimiter(OK_2).isPresent());
    }

    @Test
    void getDelimiter_given_one_one_two_should_be_absent() {
        assertFalse(StringCalculatorParser.getDelimiter(OK_4).isPresent());
    }

    @Test
    void getDelimiter_given_one_two_three_four_five_should_be_absent() {
        assertFalse(StringCalculatorParser.getDelimiter(OK_15).isPresent());
    }

    @Test
    void getDelimiter_given_one_two_three_with_newline_should_be_absent() {
        assertFalse(StringCalculatorParser.getDelimiter(OK_6).isPresent());
    }

    @Test
    void getDelimiter_given_one_two_with_semicolon_should_be_present() {
        assertTrue(StringCalculatorParser.getDelimiter(OK_3).isPresent());
    }

    @Test
    void getDelimiter_invalid_value_with_no_delimiter_should_be_absent() {
        assertFalse(StringCalculatorParser.getDelimiter(NOT_OK).isPresent());
    }

    @Test
    void hasDelimiterDefinition_one_one_with_no_delimiter_should_return_false() {
        assertFalse(StringCalculatorParser.hasDelimiterDefinition(OK_2));
    }

    @Test
    void hasDelimiterDefinition_one_two_with_semicolon_should_return_true() {
        assertTrue(StringCalculatorParser.hasDelimiterDefinition(OK_3));
    }
}