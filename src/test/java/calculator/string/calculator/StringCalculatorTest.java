package calculator.string.calculator;

import calculator.exceptions.MalformedInputException;
import calculator.exceptions.NegativeNumberException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringCalculatorTest {

    StringCalculator instance = new StringCalculator();

    static final String EMPTY = "";
    static final String OK_1 = "1";
    static final String OK_2 = "1,1";
    static final String OK_3 = "//;\n1;2";
    static final String OK_4 = "1,1,2";
    static final String OK_6 = "1\n2,3";
    static final String OK_15 = "1,2,3,4,5";
    static final String NOT_OK = "1,\n";
    static final String NOT_OK_NEGATIVE = "-1,2";

    @Test
    void add_empty_string_should_return_0() {
        assertEquals(0, instance.add(EMPTY));
    }

    @Test
    void add_only_one_number_should_return_itself() {
        assertEquals(1, instance.add(OK_1));
    }


    @Test
    void add_one_one_should_return_2() {
        assertEquals(2, instance.add(OK_2));
    }

    @Test
    void add_one_two_with_delimiter_should_return_3() {
        assertEquals(3, instance.add(OK_3));
    }

    @Test
    void add_one_one_two_should_return_4() {
        assertEquals(4, instance.add(OK_4));
    }

    @Test
    void add_one_two_three_with_newline_should_return_6() {
        assertEquals(6, instance.add(OK_6));
    }


    @Test
    void add_one_two_three_four_five_should_return_15() {
        assertEquals(15, instance.add(OK_15));
    }


    @Test
    void add_incorrect_value_should_throw_malformed_input_exception() {
        Assertions.assertThrows(MalformedInputException.class, () -> {
            instance.add(NOT_OK);
        });
    }

    @Test
    void add_negative_value_should_throw_negative_input_exception() {
        Assertions.assertThrows(NegativeNumberException.class, () -> {
            instance.add(NOT_OK_NEGATIVE);
        });
    }


}