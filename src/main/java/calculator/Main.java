package calculator;

import calculator.string.calculator.StringCalculator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.util.Arrays;

public class Main {

    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void run() throws RuntimeException {
        StringCalculator sc = new StringCalculator();

        Arrays.asList("", "1", "1,2", "1\n2,3", "//;\n1;2", "1,\n", "-1,2", "-1,-2", "-8","//@@\n1@@2")
                .forEach(input -> {
                    try {
                        logger.info(" | input | " + input );
                        logger.info(" | result | " +  sc.add(input));
                        logger.info(" --------------------------------- ");
                    } catch (RuntimeException e) {
                        logger.error(" | exception | " +  e.getMessage());
                        logger.info(" --------------------------------- ");
                    }

                });

    }


    public static void main(String[] args) {
        run();
    }
}
