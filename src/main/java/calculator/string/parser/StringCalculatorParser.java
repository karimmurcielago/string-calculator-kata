package calculator.string.parser;

import calculator.exceptions.MalformedInputException;

import java.util.*;
import java.util.stream.Collectors;

import static calculator.string.validator.StringCalculatorValidator.validate;

public class StringCalculatorParser {

    private static final String DEFAULT_DELIMITER = ",";


    /**
     *  Return a list of Integer represented by the encoded input
     * @param numbersToParse
     * @return
     * @throws MalformedInputException
     */
    public static List<Integer> parse(String numbersToParse) throws MalformedInputException {

        String givenDelimiter = getDelimiter(numbersToParse).orElse(DEFAULT_DELIMITER);
        String numbers = removeDelimiterDefinition(numbersToParse);
        List<String> delimiters = Arrays.asList("\n", givenDelimiter);
        List<String> tokens = tokenize(numbers, delimiters);

        validate(tokens);

        return tokens.stream()
                .filter(token -> !token.equals("\n") && !token.equals(givenDelimiter))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }


    /**
     *  This function tokenize the input and keeps the delimiters
     *
     * @param numbers
     * @param delimiters
     * @return List of tokens
     */
    public static List<String> tokenize(String numbers, List<String> delimiters) {
        StringTokenizer st = new StringTokenizer(numbers, String.join(",", delimiters), true);
        return Collections.list(st).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());
    }

    /**
     * The delimiter is given or not in the first 4 chars
     * @param numbers
     * @return
     */
    public static String removeDelimiterDefinition(String numbers) {
        return hasDelimiterDefinition(numbers) ? numbers.substring(4) : numbers;
    }

    // A Delimiter is supposed to be declared with : //[delimiter]\n[numbers…]”
    public static boolean hasDelimiterDefinition(String numbers) {
        return numbers.length() >= 3 & numbers.charAt(0) == '/' & numbers.charAt(1) == '/';
    }

    // A delimiter is optional and should be given at the styart of the input
    public static Optional<String> getDelimiter(String numbers) {
        return hasDelimiterDefinition(numbers) ? Optional.of(String.valueOf(numbers.charAt(2))) : Optional.empty();
    }

}
