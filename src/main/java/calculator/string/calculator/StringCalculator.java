package calculator.string.calculator;

import calculator.Calculator;
import calculator.string.parser.StringCalculatorParser;

public class StringCalculator implements Calculator {

    @Override
    public int add(String numbers) {
        if (numbers == null) return -1;
        if (numbers.isEmpty()) return 0;
        if (numbers.matches("[0-9]*")) return Integer.parseInt(numbers);
        return StringCalculatorParser.parse(numbers).stream().reduce(0, Integer::sum);
    }
}
