package calculator.string.validator;

import calculator.exceptions.MalformedInputException;
import calculator.exceptions.NegativeNumberException;

import java.util.List;
import java.util.ListIterator;

import static calculator.string.helper.StringCalculatorHelper.getNegatives;
import static calculator.string.helper.StringCalculatorHelper.isPositiveNumber;

public class StringCalculatorValidator {

    public static void validate(List<String> tokens) {
        AssertValidSyntext(tokens);
        AssertNoNegativeNumbers(tokens);
    }

    private static void AssertNoNegativeNumbers(List<String> tokens) {
        List<String> negatives = getNegatives(tokens);
        if (!negatives.isEmpty()) throw new NegativeNumberException(negatives);
    }

    private static void AssertValidSyntext(List<String> tokens) {
        if (isMalformed(tokens)) throw new MalformedInputException();
    }

    /**
     * Is malformed when the a delimiter is not surrounded with numbers
     * @param tokens
     * @return
     */
     static boolean isMalformed(List<String> tokens) {

        ListIterator<String> listIterator = tokens.listIterator();

        while (listIterator.hasNext()) {
            String previous = listIterator.hasPrevious() ? tokens.get(listIterator.previousIndex()) : null;
            String current = listIterator.next();
            String next = listIterator.hasNext() ? tokens.get(listIterator.nextIndex()) : null;
            if ((current.equals("\n") && (!isPositiveNumber(previous)) && !isPositiveNumber(next))) {
                return true;
            }
        }

        return false;
    }



}
