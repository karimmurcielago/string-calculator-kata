package calculator.string.helper;

import java.util.List;
import java.util.stream.Collectors;

public class StringCalculatorHelper {

    private StringCalculatorHelper(){}

    private static final String NUMBER_REGEXP = "^[0-9]+$";
    private static final String NEGATIVE_NUMBER_REGEXP = "^-[0-9]+$";

    public static boolean isPositiveNumber(String s) {
        if (s == null) return false;
        return s.matches(NUMBER_REGEXP);
    }

    public static boolean isNegativeNumber(String s) {
        if (s == null) return false;
        return s.matches(NEGATIVE_NUMBER_REGEXP);
    }

    public static List<String> getNegatives(List<String> tokens) {
        return tokens.stream().filter(StringCalculatorHelper::isNegativeNumber).collect(Collectors.toList());
    }



}
