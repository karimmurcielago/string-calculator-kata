package calculator.exceptions;

public class MalformedInputException extends IllegalArgumentException {
    public MalformedInputException() {
        super("Malformed input");
    }
}