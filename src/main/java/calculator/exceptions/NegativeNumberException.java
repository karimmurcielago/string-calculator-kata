package calculator.exceptions;

import java.util.List;

public class NegativeNumberException extends IllegalArgumentException {
        public NegativeNumberException(List<String> negatives) {
            super("negatives not allowed :" +  String.join(",",negatives));
        }
    }