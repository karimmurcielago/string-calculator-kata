package calculator;

import calculator.exceptions.MalformedInputException;
import calculator.exceptions.NegativeNumberException;

public interface Calculator {

    /**
     * @param numbers . A String following a pattern
     * @return integer . The result of the the given encoded input
     * @throws MalformedInputException when the format of the input is incorrect
     * @throws NegativeNumberException chen the input contains negative value(s)
     */
    int add(String numbers) throws MalformedInputException, NegativeNumberException;
}
